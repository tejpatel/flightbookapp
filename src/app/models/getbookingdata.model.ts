export interface GetBookingData {
  _id: string;
  tripType: string;
  fromPlace: string;
  toPlace: string;
  travelDate: string;
  returnDate: string;
  adultsCount: number;
  childrenCount: number;
  infantsCount: number;
  travelClass: string;
  userId: string;
  status: string;
  __v: number;
}
