import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HeaderComponent } from "./common/header/header.component";
import { FooterComponent } from "./common/footer/footer.component";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";

import { AutheticationService } from "./service/authetication.service";
import { AlertService } from "./service/alert.service";
import { RouterModule } from "@angular/router";
import { HomeComponent } from "./staticpages/home/home.component";
import { PagenotfoundComponent } from "./staticpages/pagenotfound/pagenotfound.component";
import { FlightBookComponent } from "./flight/flight-book/flight-book.component";
import { BookinghistoryComponent } from "./staticpages/bookinghistory/bookinghistory.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TokenInterceptor } from "./token-interceptor";
import { ToastrModule } from "ngx-toastr";
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxPaginationModule } from "ngx-pagination";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { FilterItemPipe } from './pipes/filter-item.pipe';

const appRoute = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  { path: "bookhistory", component: BookinghistoryComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "**", component: PagenotfoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    PagenotfoundComponent,
    FlightBookComponent,
    BookinghistoryComponent,
    FilterItemPipe
  ],
  imports: [
    Ng2SearchPipeModule,
    NgxPaginationModule,
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoute),
    BrowserModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,

    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: "toast-top-right",
      preventDuplicates: true,
      disableTimeOut: true,
      closeButton: true
    })
  ],
  providers: [
    AutheticationService,
    AlertService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
