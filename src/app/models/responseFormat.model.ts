import { Login } from "./login.model";

export interface ResponseFormatModel<T> {
  success: string;
  data: T;
}
