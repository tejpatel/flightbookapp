import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { FlightBookComponent } from "./flight-book.component";
import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { BookingService } from 'src/app/service/booking.service';
import { AutheticationService } from 'src/app/service/authetication.service';

describe("FlightBookComponent", () => {
  let component: FlightBookComponent;
  let fixture: ComponentFixture<FlightBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlightBookComponent],
      imports: [
        ReactiveFormsModule,
        FormGroup,
        FormData,
        FormControl,
        Validators,
        FormsModule,
        RouterModule,
        
      ],
      providers:[AutheticationService,BookingService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
