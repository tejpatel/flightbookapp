import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AutheticationService } from "src/app/service/authetication.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  constructor(
    private _authservice: AutheticationService,
    private routes: Router,
    private _toaster: ToastrService
  ) {}

  loggeduser: boolean = false;
  ngOnInit() {
    // else this.isEnabled = false;
  }
  ngAfterContentChecked() {
    if (this._authservice.getToken() !== null) {
      this.loggeduser = true;
    } else {
      this.loggeduser = false;
    }
    // console.log(this._authservice.getToken());
  }
  onLogin() {
    // this.loggeduser = false;

    // console.log(this.loggeduser)
    this.routes.navigate(["/login"]);
    this.loggeduser = this._authservice.logged;
    // this.loggeduser = false;
  }
  onLogout() {
    this.loggeduser = false;
    this._toaster.error("Logout Successfully");
    // alert("Logout Successfully");
    this._authservice.removeToken();

    this.routes.navigate(["/home"]);
  }
}
