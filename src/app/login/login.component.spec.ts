import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

import { LoginComponent } from "./login.component";
import { NO_ERRORS_SCHEMA } from "@angular/compiler/src/core";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import {
  ReactiveFormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormsModule
} from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        ReactiveFormsModule,
        FormGroup,
        FormData,
        FormControl,
        Validators,
        FormsModule,
        RouterModule
      ],
      providers:[ToastrService],

      schemas: [CUSTOM_ELEMENTS_SCHEMA]
      // schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it("should create", () => {
  //   expect(component).toBeTruthy();
  // });

  it("should not show submitted value is set out", () => {
    expect(component.submitted).toBe(false);
  });

  it("shoould containe a default value for Login Form", () => {
    expect(component.loginform.value).toEqual({ email: "tej", password: "" });
  });
});
