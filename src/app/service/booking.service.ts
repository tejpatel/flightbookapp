import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AutheticationService } from "./authetication.service";
import { Observable } from "rxjs";
import { GetBookingData } from "../models/getbookingdata.model";
import { ResponseFormatModel } from "../models/responseFormat.model";

@Injectable({
  providedIn: "root"
})
export class BookingService {
  constructor(
    private http: HttpClient,
    private _authservice: AutheticationService
  ) {}

  bookFlight(
    tripTypeparameter,
    fromPlaceparameter,
    toPlaceparameter,
    travelDateparameter,
    returnDateparameter,
    adultsCountparameter,
    childrenCountparameter,
    infantsCountparameter,
    travelClassparameter
  ) {
    let httpHeader = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("x-access-token", this._authservice.getToken());
    let options = {
      headers: httpHeader
    };

    const data = {
      tripType: tripTypeparameter,
      fromPlace: fromPlaceparameter,
      toPlace: toPlaceparameter,
      travelDate: travelDateparameter,
      returnDate: returnDateparameter,
      adultsCount: adultsCountparameter,
      childrenCount: childrenCountparameter,
      infantsCount: infantsCountparameter,
      travelClass: travelClassparameter
    };

    return this.http.post(
      "http://10.12.46.19:3000/flights/bookFlight",
      data,
      options
    );
  }

  getBookingData(): Observable<ResponseFormatModel<GetBookingData[]>> {
    let httpHeader = new HttpHeaders().set("Content-Type", "application/json");
    // .set("x-access-token", this._authservice.getToken());
    let options = {
      headers: httpHeader
    };
    return this.http.post<ResponseFormatModel<GetBookingData[]>>(
      "http://10.12.40.85:3000/flights/getBookingsData",
      "",
      options
    );
  }

  cancelBooking(id) {
    let httpheader = new HttpHeaders().set("Content-Type", "application/json");
    // .set("x-access-token", this._authservice.getToken());
    let options = {
      headers: httpheader
    };
    return this.http.post(
      "http://10.12.40.85:3000/flights/cancelFlight",
      { bookingId: id },
      options
    );
  }
}
