import {
  HttpInterceptor,
  HttpHeaders,
  HttpHeaderResponse
} from "@angular/common/http";
import { tap } from "rxjs/operators";

export class TokenInterceptor implements HttpInterceptor {
  intercept(
    req: import("@angular/common/http").HttpRequest<any>,
    next: import("@angular/common/http").HttpHandler
  ): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {
    console.log("via interceptor");

    const req1 = "http://10.12.40.85:3000/users/login";
    if (req.url.search(req1) === -1) {
      const token = localStorage.getItem("token");
      console.log(token);
      let clone_req = req.clone({
        headers: new HttpHeaders().append("x-access-token", token)
      });
      return next.handle(clone_req).pipe(
        tap(
          event => {},
          error => {
            if (error instanceof HttpHeaderResponse)
              console.log("this is error" + error);
          }
        )
      );
    } else {
      return next.handle(req);
    }
  }
}
