import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterItem"
})
export class FilterItemPipe implements PipeTransform {
  transform(value: any, searchItem: any) {
    if (!value || !searchItem) {
      return value;
    } else {
       for (var i=0; i < value.length; i++) {
        if (value[i].name === searchItem) {
            return value[i];
        }
    }
      // return value.filter(data => {
        
      //   // Object.keys(data).find(item => {
      //   //   console.log(data[item].includes(searchItem));
      //   //   return data[item].includes(searchItem);
      //   // })
      // });
    }
    // return null;
  }
}
