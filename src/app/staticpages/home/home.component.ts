import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { data } from "src/app/inputCollection.json";
import * as $ from "jquery";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    data;
    var elements = ["span", "div", "h1", "h2", "h3"];
    $("#textarea2").textcomplete({
      html: {
        match: /<(\w*)$/,
        search: function(term, callback) {
          callback(
            $.map(elements, function(element) {
              return element.indexOf(term) === 0 ? element : null;
            })
          );
        },
        index: 1,
        replace: function(element) {
          return ["<" + element + ">", "</" + element + ">"];
        }
      }
    });
  }
}
