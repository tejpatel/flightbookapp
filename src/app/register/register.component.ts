import { Component, OnInit } from "@angular/core";
import {
  ReactiveFormsModule,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { AutheticationService } from "../service/authetication.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  constructor(
    private _authservice: AutheticationService,
    private router: Router
  ) {}

  registerform: FormGroup;
  isSubmit = false;

  ngOnInit() {
    this.registerform = new FormGroup({
      first_name: new FormControl(null, Validators.required),
      last_name: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      confrim_password: new FormControl(null, Validators.required),
      gender: new FormControl(null, Validators.required),
      mobile_number: new FormControl(null, Validators.required)
    });
  }

  onRegisterUser(registerform) {
    console.log(registerform);
    this._authservice
      .createUser(
        registerform.value.email,
        registerform.value.password,
        registerform.value.first_name,
        registerform.value.last_name,
        registerform.value.mobile_number,
        registerform.value.gender
      )
      .subscribe(
        data => {
          alert(JSON.stringify(data));
        },
        error => {
          console.log(error);
        }
      );
    this.router.navigate(["/login"]);
  }

  onClose() {
    this.router.navigate(["/"]);
  }
}
